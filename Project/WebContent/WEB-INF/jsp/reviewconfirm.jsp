<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>口コミ修正</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- 自分で作成したCSS -->
<link rel="stylesheet" href="css/style.css">
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="https://unpkg.com/@popperjs/core@2" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript">
	var target = "";

	// 設定終了

	function jump() {

		var url = document.form1.select.options[document.form1.select.selectedIndex].value;

		if (url != "") {

			if (target == 'top') {

				top.location.href = url;

			} else if (target == 'blank') {

				window.open(url, 'window_name');

			} else if (target != "") {

				eval('parent.' + target + '.location.href = url');

			} else {

				location.href = url;

			}

		}

	}
</script>

</head>

<body class="wrapper bg-granite">

	<!-- header -->
	<div class="header sample1">
		<ul class="nav justify-content-end">
			<li class="nav-item"><a class="nav-link active" href="Cart"><img
					src="img/greencart.jpg" width="40" height="40"></a></li>
			<li class="nav-item"><a class="nav-link" href="Login"><img
					src="img/userlogo.png" class="carvs" width="40" height="40"></a>
			</li>
			<li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a></li>
		</ul>
	</div>

	<!--　navbar　-->
	<div>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<!-- Image and text -->
			<nav class="navbar navbar-light bg-light">
				<a class="navbar-brand" href="Index"> <img
					src="img/smalltreelogo.png" width="30" height="30"
					class="d-inline-block align-top" alt="logo"> Healthy
					Lifestyle
				</a>
			</nav>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="Error">ブログ
							<span class="sr-only">(current)</span>
					</a></li>

					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> 商品カテゴリ </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="ProductList?categoryId=1">新鮮食品</a>
							<a class="dropdown-item" href="ProductList?categoryId=2">米・穀類</a>
							<a class="dropdown-item" href="ProductList?categoryId=3">調味料</a>
							<a class="dropdown-item" href="ProductList?categoryId=4">お菓子</a>
							<a class="dropdown-item" href="ProductList?categoryId=5">その他</a>
						</div></li>

					<li class="nav-item"><a class="nav-link" href="#"></a></li>

				</ul>
				<form method="post" action="Search" class="form-inline my-2 my-lg-0">
					<input class="form-control mr-sm-2" type="text"
						placeholder="Search" aria-label="Search" id="search" name="search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>
			</div>
		</nav>
	</div>


	<!--　reviewconfirm form　-->
	<div class="sim-form bg-br-green">
		<div class="text-center" style="padding-bottom: 20px">
			<img src="img/review.png" alt="review" class="carvs" width="200px"
				height="200px">
		</div>
		    <c:if test = "${errMsg != null}" >
		    	<div class="alert alert-success" role="alert">
		 		${errMsg}
				</div>
			</c:if>

		<div style="text-align: center;">
			<h3>この内容で投稿します</h3>

		</div>


		<div style="text-align: center; padding-bottom: 20px;">
			<div class="surround" style="width: 70%;">
				<div>
					<h3>レビュー</h3>
				</div>

				<form method="get" action="ReviewFix">
					<div class="form-group">
						<label for="reviewProduct">商品名</label> <input type="text"
							class="form-control" id="title" name="title" placeholder="${title}">
					</div>
					<div class="form-group">
						<label for="exampleFormControlTextarea1">内容</label>
						<textarea class="form-control" id="exampleFormControlTextarea1"name="reviewDetail"
							placeholder="${title}" rows="3"></textarea>
					</div>
					<div class="form-group">
						<label for="reviePicture">写真を載せる</label> <input type="file"
							class="form-control-file" id="reviePicture">
					</div>
					<div class="form-group">
						<label for="reviewProduct">商品番号</label> <input type="text"
							class="form-control" id="productId" name="productId"placeholder="商品番号">
					</div>
					<button type="submit" class="btn btn-outline-danger"><a href="ReviewFix">修正</a></button>
					<button type="button" class="btn btn-outline-success"><a href="ReviewResult">確認</a></button>
				</form>
			</div>
		</div>
	</div>


	<!--　footer　-->
	<div class="bg-w-green text-center"
		style="padding-top: 10px; padding-bottom: 10px;">
		<img src="img/facebooklogo.png" class="carvs" width="40" height="40">
		<img src="img/instagramlogo.png" class="carvs" width="40" height="40">
		<img src="img/twitterlogo.png" class="carvs" width="40" height="40">

	</div>
	<footer class="sample1">
		<div>
			<div>
				<ul>
					<li><a href="Index" class="col-sm list-group-item-action">・ホーム</a></li>
					<li><a href="Error" class="col-sm list-group-item-action">・ブログ</a></li>
					<li><a href="Review" class="col-sm list-group-item-action">・口コミ</a></li>
					<li><a href="Error" class="col-sm list-group-item-action">・お問い合わせ</a></li>
				</ul>
			</div>
		</div>

		<div class="text-center">&copy;Live Health Life inc, 2020.</div>
	</footer>

</body>

</html>
