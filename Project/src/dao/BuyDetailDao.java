package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.BuyDetailBeans;
import beans.ProductBeans;

public class BuyDetailDao {
	public static void insertBuyDetail(BuyDetailBeans buyDetail) {

		Connection conn = null;
		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "INSERT INTO buy_detail(buy_id, product_id) VALUES(?, ?)";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, buyDetail.getBuyId());
			pStmt.setInt(2, buyDetail.getProductId());
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//購入IDから商品詳細をだす
	public static List<ProductBeans> getBuyDataListFromBuyId(int buyId) {

		Connection conn = null;
		try {

			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "SELECT product.id, product.name, product.price"
					+ " FROM buy_detail"
					+ " JOIN product"
					+ " ON buy_detail.product_id = product.id"
					+ " WHERE buy_detail.buy_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, buyId);
			ResultSet rs = pStmt.executeQuery();

			List<ProductBeans> buyDetailProductList = new ArrayList<ProductBeans>();

			while(rs.next()) {
				ProductBeans pb = new ProductBeans();
				pb.setId(rs.getInt("id"));
				pb.setName(rs.getString("name"));
				pb.setPrice(rs.getInt("price"));

				buyDetailProductList.add(pb);
			}

			return buyDetailProductList;

			} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static List<ProductBeans> getProductDetailFromId(int id) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}


}
