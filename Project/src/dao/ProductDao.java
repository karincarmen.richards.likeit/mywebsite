package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ProductBeans;

public class ProductDao {

	//カテゴリーIDから商品検索
	public List<ProductBeans> findByCategoryId(String categoryId) {
		List<ProductBeans> productList = new ArrayList<ProductBeans>();
		Connection conn = null;
		try {

			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文実行
			String sql = "SELECT * FROM product WHERE category_id = ?";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, categoryId);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String detail = rs.getString("detail");
				int price = rs.getInt("price");
				String fileName = rs.getString("file_name");
				ProductBeans product = new ProductBeans(id, name, detail, price, fileName);

				productList.add(product);
			}

			} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return productList;
	}

	//商品マスタをリストで返す
		public List<ProductBeans> findAll(){
			Connection conn = null;
			List<ProductBeans> masterList = new ArrayList<ProductBeans>();

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				// TODO: 未実装：管理者以外を取得するようSQLを変更する
				String sql = "SELECT id, name, stock FROM product";

				// SELECTを実行し、結果表を取得
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				// 結果表に格納されたレコードの内容を
				// Userインスタンスに設定し、ArrayListインスタンスに追加
				while (rs.next()) {
					int id = rs.getInt("id");
					String name = rs.getString("name");
					int stock = rs.getInt("stock");
					ProductBeans product = new ProductBeans(id, name, stock);

					masterList.add(product);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return masterList;
		}

		//商品マスタ詳細表示
		public ProductBeans masterDetail(String id) {

			Connection conn = null;
			try {

				// データベース接続
				conn = DBManager.getConnection();

				// SELECT文は実行してからコピペ
				String sql = "SELECT * FROM product WHERE id = ?";


				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, id);
				ResultSet rs = pStmt.executeQuery();

				if(!rs.next()) {
					return null;
				}

					int idData = rs.getInt("id");
					String nameData = rs.getString("name");
					String detailData = rs.getString("detail");
					int stockData = rs.getInt("stock");
					int costData = rs.getInt("cost");
					int priceData = rs.getInt("price");
					String fileName = rs.getString("file_name");
					int supplierIdData = rs.getInt("supplier_id");
					int categoryIdData = rs.getInt("category_id");

					return new ProductBeans(idData, nameData, detailData, stockData, costData, priceData, fileName, supplierIdData, categoryIdData);

				} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

		//新規マスタ登録
		public int registMaster(String name, String detail, String stock, String cost, String price, String fileName, String supplierId, String categoryId){
			int resultNum = 0;
			Connection conn = null;
			try {
				// データベース接続
				conn = DBManager.getConnection();

				// SELECT文は実行してからコピペ
				String sql = "INSERT INTO product(name, detail, stock, cost, price, file_name, supplier_id, category_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";


				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, name);
				pStmt.setString(2, detail);
				pStmt.setString(3, stock);
				pStmt.setString(4, cost);
				pStmt.setString(5, price);
				pStmt.setString(6, fileName);
				pStmt.setString(7, supplierId);
				pStmt.setString(8, categoryId);
				resultNum = pStmt.executeUpdate();

				return resultNum;


			} catch (SQLException e) {
				e.printStackTrace();
				return resultNum;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return resultNum;
					}
				}
			}
		}


		//マスタ削除
		public int masterDelete(String id) {

			Connection conn = null;
			int resultNum = 0;

			try {
				// データベース接続
				conn = DBManager.getConnection();

				// SELECT文は実行してからコピペ
				String sql = "DELETE FROM product WHERE id = ?";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, id);
				resultNum = pStmt.executeUpdate();

				return resultNum;

			} catch (SQLException e) {
				e.printStackTrace();
				return resultNum;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return resultNum;
					}

				}
			}
		}

		//商品マスタのアップデート
		public int masterUpdate(String id, String name, String detail, String stock, String cost, String price, String fileName, String supplierId, String categoryId) {

			Connection conn = null;
			int resultNum = 0;

			try {
				// データベース接続
				conn = DBManager.getConnection();

				// SELECT文は実行してからコピペ

				String sql = "UPDATE product SET name= ?, detail= ?, stock= ?, cost= ?, price= ?, file_name= ?, supplier_id= ?, category_id= ? WHERE id=?";


				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, name);
				pStmt.setString(2, detail);
				pStmt.setString(3, stock);
				pStmt.setString(4, cost);
				pStmt.setString(5, price);
				pStmt.setString(6, fileName);
				pStmt.setString(7, supplierId);
				pStmt.setString(8, categoryId);
				pStmt.setString(9, id);
				resultNum = pStmt.executeUpdate();

				return resultNum;

			} catch (SQLException e) {
				e.printStackTrace();
				return resultNum;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return resultNum;
					}

				}
			}
		}

		//商品検索
		public List<ProductBeans> findProductByName(String searchWord){
				Connection conn = null;
				ResultSet rs = null;
				List<ProductBeans> productList = new ArrayList<ProductBeans>();

				try {
					conn = DBManager.getConnection();


					if(searchWord.length() == 0) {
						System.out.println("IF文通ってる");
						//全件検索
						conn = DBManager.getConnection();
						String sql = "SELECT * FROM product ORDER BY id ASC";
						Statement stmt = conn.createStatement();
						rs = stmt.executeQuery(sql);


					}else {
						System.out.println("ELSE文通ってる");
						//商品名検索
						String sql = "SELECT*FROM product WHERE name LIKE '%" + searchWord + "%' ORDER BY id ASC";
						Statement stmt = conn.createStatement();
						rs = stmt.executeQuery(sql);


					}

					while (rs.next()) {
						int idData = rs.getInt("id");
						String nameData = rs.getString("name");
						String detailData = rs.getString("detail");
						int stockData = rs.getInt("stock");
						int costData = rs.getInt("cost");
						int priceData = rs.getInt("price");
						String fileName = rs.getString("file_name");
						int supplierIdData = rs.getInt("supplier_id");
						int categoryIdData = rs.getInt("category_id");
						ProductBeans product = new ProductBeans(idData, nameData, detailData, stockData, costData, priceData, fileName, supplierIdData, categoryIdData);
						productList.add(product);
					}



				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				} finally {
					// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}
				return productList;
		}
}
