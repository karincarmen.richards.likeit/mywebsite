package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserBeans;

public class UserDao {

	public UserBeans findByLoginId(String loginId, String password) {
		Connection conn = null;
		try {

			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "SELECT * FROM n_user WHERE login_id = ? and password = ?";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("user_name");
			Date birthDateData = rs.getDate("birth_date");
			String addressData = rs.getString("address");
			String passwordData = rs.getString("password");
			return new UserBeans(idData, loginIdData, nameData, birthDateData, addressData, passwordData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public UserBeans userDetailInfo(String id) {
		Connection conn = null;
		try {

			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "SELECT * FROM n_user WHERE id = ?";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("user_name");
			Date birthDateData = rs.getDate("birth_date");
			String addressData = rs.getString("address");
			String passwordData = rs.getString("password");
			return new UserBeans(idData, loginIdData, nameData, birthDateData, addressData, passwordData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	//ユーザ登録
		public int registUser(String loginId, String name, String birthdate, String address, String password){
			int resultNum = 0;
			Connection conn = null;

			String result = invisiblePass(password);

			try {
				// データベース接続
				conn = DBManager.getConnection();

				// SELECT文は実行してからコピペ
				String sql = "INSERT INTO n_user(login_id, user_name, birth_date, address, password) VALUES(?, ?, ?, ?, ?)";


				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				pStmt.setString(2, name);
				pStmt.setString(3, birthdate);
				pStmt.setString(4, address);
				pStmt.setString(5, result);
				resultNum = pStmt.executeUpdate();

				return resultNum;


			} catch (SQLException e) {
				e.printStackTrace();
				return resultNum;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return resultNum;
					}
				}
			}
		}

		//
		public int userUpdate(String loginId, String name, String birthdate, String address, String password) {

			Connection conn = null;
			int resultNum = 0;

			String result = invisiblePass(password);
			try {
				// データベース接続
				conn = DBManager.getConnection();

				// SELECT文は実行してからコピペ

				String sql = "UPDATE n_user SET user_name= ?, birth_date= ?, address= ?, password= ? WHERE login_id= ?";


				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, name);
				pStmt.setString(2, birthdate);
				pStmt.setString(3, address);
				pStmt.setString(4, result);
				pStmt.setString(5, loginId);
				resultNum = pStmt.executeUpdate();

				return resultNum;

			} catch (SQLException e) {
				e.printStackTrace();
				return resultNum;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return resultNum;
					}

				}
			}
		}

		public UserBeans checkId(String loginId) {
			Connection conn = null;
			try {

				// データベース接続
				conn = DBManager.getConnection();

				// SELECT文は実行してからコピペ
				String sql = "SELECT * FROM n_user WHERE login_id = ?";


				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				ResultSet rs = pStmt.executeQuery();

				if (!rs.next()) {
					return null;
				}

				// 必要なデータのみインスタンスのフィールドに追加

				String loginIdData = rs.getString("login_id");
				return new UserBeans(loginIdData);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

		public int userUpdateWithoutPass(String loginId, String name, String birthdate, String address) {
			Connection conn = null;
			int resultNum = 0;

			try {
				// データベース接続
				conn = DBManager.getConnection();

				// SELECT文は実行してからコピペ

				String sql = "UPDATE n_user SET user_name= ?, birth_date= ?, address= ? WHERE login_id= ?";


				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, name);
				pStmt.setString(2, birthdate);
				pStmt.setString(3, address);
				pStmt.setString(4, loginId);
				resultNum = pStmt.executeUpdate();

				return resultNum;

			} catch (SQLException e) {
				e.printStackTrace();
				return resultNum;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return resultNum;
					}

				}
			}
		}


		//暗号化
		public static String invisiblePass(String password) {

			//ハッシュを生成したい元の文字列
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = null;

			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				String result = DatatypeConverter.printHexBinary(bytes);
				//標準出力
				System.out.println(result);
				return result;

			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				return null;
			}

		}
}
