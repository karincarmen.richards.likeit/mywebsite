package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBManager;
import beans.ReviewBeans;

public class ReviewDao {

	//レビューの投稿
	public int registReview(String title, String detail, String fileName, String productId){
		int resultNum = 0;
		Connection conn = null;
		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "INSERT INTO review(title, detail, file_name, product_id, create_date) VALUES(?, ?, ?, ?,NOW())";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, title);
			pStmt.setString(2, detail);
			pStmt.setString(3, fileName);
			pStmt.setString(4, productId);
			resultNum = pStmt.executeUpdate();

			return resultNum;


		} catch (SQLException e) {
			e.printStackTrace();
			return resultNum;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return resultNum;
				}
			}
		}
	}

	//レビューのタイトルをリストで返す
	public List<ReviewBeans> findAll(){
		Connection conn = null;
		List<ReviewBeans> reviewList = new ArrayList<ReviewBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT id, title FROM review";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				ReviewBeans review = new ReviewBeans(id, title);

				reviewList.add(review);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return reviewList;
	}

	//レビューの詳細
	public ReviewBeans reviewDetail(String id){
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT* FROM review WHERE id=?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			String titleData = rs.getString("title");
			String detailData = rs.getString("detail");
			String fileNameData = rs.getString("file_name");
			int productIdData = rs.getInt("product_id");
			Date createDateData = rs.getDate("create_date");
			return new ReviewBeans(titleData, detailData, fileNameData, productIdData, createDateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//レビュー削除
	public int reviewDelete(String id) {

		Connection conn = null;
		int resultNum = 0;

		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "DELETE FROM review WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			resultNum = pStmt.executeUpdate();

			return resultNum;

		} catch (SQLException e) {
			e.printStackTrace();
			return resultNum;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return resultNum;
				}

			}
		}
	}

}
