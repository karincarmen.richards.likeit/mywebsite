package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.DeliveryMethodBeans;

public class DeliveryMethodDao {

	//
	public List<DeliveryMethodBeans> getDeliveryList(){
		List<DeliveryMethodBeans> deliveryMethodList = new ArrayList<DeliveryMethodBeans>();
		Connection conn = null;
		try {

			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "SELECT * FROM delivery_method";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				DeliveryMethodBeans delivery = new DeliveryMethodBeans(id, name, price);

				deliveryMethodList.add(delivery);
			}

			} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return deliveryMethodList;
	}


	//配送IDからデータ詳細を戻す
	public DeliveryMethodBeans getDBMethodFromId(int id) {

		Connection conn = null;
		try {

			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "SELECT * FROM delivery_method WHERE id = ?";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			int idData = rs.getInt("id");
			String nameData = rs.getString("name");
			int priceData = rs.getInt("price");
			return new DeliveryMethodBeans(idData, nameData, priceData);

			} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}
