package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.BuyBeans;
import beans.ProductBeans;

public class BuyDao {
	//
	public static int getTotalPrice(ArrayList<ProductBeans> products) {

		int total = 0;
		for(ProductBeans product : products) {
			total += product.getPrice();
		}
		return total;
	}

	public static int insertBuy(BuyBeans buyData) {

		Connection conn = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			st  = conn.prepareStatement(
					"INSERT INTO buy(total_price, create_date, delivery_method_id,user_id) VALUES(?, NOW(), ?, ?)",
					Statement.RETURN_GENERATED_KEYS);


			// SELECTを実行し、結果表を取得
			st.setInt(1, buyData.getTotalPrice());
			st.setInt(2, buyData.getDeliveryMethodId());
			st.setInt(3, buyData.getUserId());
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();

			if(rs.next()) {
				autoIncKey = rs.getInt(1);
			}

			return autoIncKey;

		} catch (SQLException e) {
			e.printStackTrace();
			return autoIncKey;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return autoIncKey;
				}
			}
		}
	}
	//buyIDから商品検索
		public static BuyBeans getBuyDataFromById(int buyId) {

			Connection conn = null;
			try {

				// データベース接続
				conn = DBManager.getConnection();

				// SELECT文は実行してからコピペ
				String sql = "SELECT * FROM buy"
						+ " JOIN delivery_method"
						+ " ON buy.delivery_method_id = delivery_method.id"
						+ " WHERE buy.id = ?";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1, buyId);
				ResultSet rs = pStmt.executeQuery();

				BuyBeans bb = new BuyBeans();
				if(rs.next()) {
					bb.setId(rs.getInt("id"));
					bb.setTotalPrice(rs.getInt("total_price"));
					bb.setCreateDate(rs.getDate("create_date"));
					bb.setDeliveryMethodId(rs.getInt("delivery_method_id"));
					bb.setUserId(rs.getInt("user_id"));
					bb.setDeliveryMethodPrice(rs.getInt("price"));
					bb.setDeliveryMethodName(rs.getString("name"));
				}

				return bb;

				} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

		//商品検索
				public static List<BuyBeans> getbuyHistoryFromByuserId(int userId){
						Connection conn = null;
						ResultSet rs = null;
						List<BuyBeans> historyList = new ArrayList<BuyBeans>();

						try {
							conn = DBManager.getConnection();



								//全件検索
								conn = DBManager.getConnection();

								String sql = "SELECT * FROM buy"
										+ " JOIN delivery_method"
										+ " ON buy.delivery_method_id = delivery_method.id"
										+ " WHERE buy.user_id = ?";

								PreparedStatement pStmt = conn.prepareStatement(sql);
								pStmt.setInt(1, userId);
								rs = pStmt.executeQuery();

								while(rs.next()) {
									BuyBeans bb = new BuyBeans();
									bb.setId(rs.getInt("id"));
									bb.setTotalPrice(rs.getInt("total_price"));
									bb.setCreateDate(rs.getDate("create_date"));
									bb.setDeliveryMethodId(rs.getInt("delivery_method_id"));
									bb.setUserId(rs.getInt("user_id"));
									bb.setDeliveryMethodPrice(rs.getInt("price"));
									bb.setDeliveryMethodName(rs.getString("name"));
									historyList.add(bb);
								}

								return historyList;



						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						} finally {
							// データベース切断
							if (conn != null) {
								try {
									conn.close();
								} catch (SQLException e) {
									e.printStackTrace();
									return null;
								}
							}
						}
				}

				public static BuyBeans getBuyInfoFromId(String buyId) {
					Connection conn = null;
					try {

						// データベース接続
						conn = DBManager.getConnection();

						// SELECT文は実行してからコピペ
						String sql = "SELECT * FROM buy"
								+ " JOIN delivery_method"
								+ " ON buy.delivery_method_id = delivery_method.id"
								+ " WHERE buy.id = ?";

						// SELECTを実行し、結果表を取得
						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setString(1, buyId);
						ResultSet rs = pStmt.executeQuery();

						BuyBeans bb = new BuyBeans();
						if(rs.next()) {
							bb.setId(rs.getInt("id"));
							bb.setTotalPrice(rs.getInt("total_price"));
							bb.setCreateDate(rs.getDate("create_date"));
							bb.setDeliveryMethodId(rs.getInt("delivery_method_id"));
							bb.setUserId(rs.getInt("user_id"));
							bb.setDeliveryMethodPrice(rs.getInt("price"));
							bb.setDeliveryMethodName(rs.getString("name"));
						}

						return bb;

						} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
				}


}
