package beans;

import java.io.Serializable;

public class ProductBeans implements Serializable{
	private int id;
	private String name;
	private String detail;
	private int stock;
	private int cost;
	private int price;
	private String fileName;
	private int supplierId;
	private int categoryId;

	//カテゴリ別で商品をリストにする
	public ProductBeans(int id, String name, String detail, int price, String fileName) {
		super();
		this.id = id;
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.fileName = fileName;
	}

	//商品マスタをリストでかえす
	public ProductBeans(int id, String name, int stock) {
		super();
		this.id = id;
		this.name = name;
		this.stock = stock;
	}

	public ProductBeans(int id, String name, int price, String fileName) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.fileName = fileName;
	}

	//新規マスタ登録
	public ProductBeans(String name, String detail, int stock, int cost, int price, String fileName, int supplierId,
			int categoryId) {
		super();
		this.name = name;
		this.detail = detail;
		this.stock = stock;
		this.cost = cost;
		this.price = price;
		this.fileName = fileName;
		this.supplierId = supplierId;
		this.categoryId = categoryId;
	}

	public ProductBeans(int id, String name, String detail, int stock, int cost, int price, String fileName,
			int supplierId, int categoryId) {
		super();
		this.id = id;
		this.name = name;
		this.detail = detail;
		this.stock = stock;
		this.cost = cost;
		this.price = price;
		this.fileName = fileName;
		this.supplierId = supplierId;
		this.categoryId = categoryId;
	}

	public ProductBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

}
