package beans;

import java.io.Serializable;
import java.util.Date;

public class ReviewBeans implements Serializable{
	private int id;
	private String title;
	private String detail;
	private String fileName;
	private Date createDate;
	private int productId;


	public ReviewBeans(int id, String title) {
		super();
		this.id = id;
		this.title = title;
	}

	public ReviewBeans(String title, String detail, String fileName, int productId, Date createDate) {
		super();
		this.title = title;
		this.detail = detail;
		this.fileName = fileName;
		this.createDate = createDate;
		this.productId = productId;
	}

	public ReviewBeans(int id, String title, String detail, String fileName, Date createDate, int productId) {
		super();
		this.id = id;
		this.title = title;
		this.detail = detail;
		this.fileName = fileName;
		this.createDate = createDate;
		this.productId = productId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}

}
