package beans;
import java.io.Serializable;
import java.util.Date;


public class UserBeans implements Serializable{
	private int id;
	private String loginId;
	private String name;
	private Date birthdate;
	private String address;
	private String password;



//ログインIDの重複確認
	public UserBeans(String loginId) {
		super();
		this.loginId = loginId;
	}

	public UserBeans(String loginId, String name, Date birthdate, String address, String password) {
		super();
		this.loginId = loginId;
		this.name = name;
		this.birthdate = birthdate;
		this.address = address;
		this.password = password;
	}

	public UserBeans(int id, String loginId, String name, Date birthdate, String address) {
		super();
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthdate = birthdate;
		this.address = address;
	}

	//ユーザ詳細を詰めてかえす
	public UserBeans(int id, String loginId, String name, Date birthdate, String address, String password) {
		super();
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthdate = birthdate;
		this.address = address;
		this.password = password;
	}

	//
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public UserBeans(String loginId, String name) {
		super();
		this.loginId = loginId;
		this.name = name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
