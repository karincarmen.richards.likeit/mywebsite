package myweb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyBeans;
import beans.BuyDetailBeans;
import beans.ProductBeans;
import dao.BuyDao;
import dao.BuyDetailDao;

/**
 * Servlet implementation class BuyResult
 */
@WebServlet("/BuyResult")
public class BuyResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		ArrayList<ProductBeans> cart = (ArrayList<ProductBeans>) session.getAttribute("cart");
		session.removeAttribute("cart");

		BuyBeans buyData = (BuyBeans) session.getAttribute("buyData");
		session.removeAttribute("buyData");

		//購入情報の登録
		int buyId = BuyDao.insertBuy(buyData);

		//購入詳細情報を購入IDに紐づけして登録
		for(ProductBeans product : cart) {
			BuyDetailBeans buyDetail = new BuyDetailBeans();
			buyDetail.setBuyId(buyId);
			buyDetail.setProductId(product.getId());
			BuyDetailDao.insertBuyDetail(buyDetail);
		}

		BuyBeans buyResult = BuyDao.getBuyDataFromById(buyId);
		request.setAttribute("buyResult", buyResult);

		List<ProductBeans> buyList = BuyDetailDao.getBuyDataListFromBuyId(buyId);
		request.setAttribute("buyList", buyList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyresult.jsp");
		dispatcher.forward(request, response);
	}

}
