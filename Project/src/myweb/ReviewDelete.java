package myweb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ReviewBeans;
import dao.ReviewDao;

/**
 * Servlet implementation class ReviewDelete
 */
@WebServlet("/ReviewDelete")
public class ReviewDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");

		ReviewDao reviewDao = new ReviewDao();
		ReviewBeans review = reviewDao.reviewDetail(id);

		request.setAttribute("id", id);
		request.setAttribute("review", review);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reviewdelete.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		ReviewDao reviewDao = new ReviewDao();
		int delete = reviewDao.reviewDelete(id);


		if(delete == 0) {
			request.setAttribute("errMsg", "削除に失敗しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reviewdelete.jsp");
			dispatcher.forward(request, response);
			return;
		}

		request.setAttribute("errMsg", "削除が完了しました");
		response.sendRedirect("Review");

	}

}
