package myweb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ProductBeans;
import dao.ProductDao;

/**
 * Servlet implementation class ProductMasterListUpdate
 */
@WebServlet("/ProductMasterListUpdate")
public class ProductMasterListUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductMasterListUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");

		ProductDao productDao = new ProductDao();
		ProductBeans produtmasterdetail = productDao.masterDetail(id);

		request.setAttribute("produtmasterdetail", produtmasterdetail);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productmasterlistupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String detail = request.getParameter("detail");
		String stock = request.getParameter("stock");
		String cost = request.getParameter("cost");
		String price = request.getParameter("price");
		String fileName = request.getParameter("fileName");
		String supplierId = request.getParameter("supplierId");
		String categoryId = request.getParameter("categoryId");


		ProductDao productDao = new ProductDao();
		int masterUpdate = productDao.masterUpdate(id, name, detail, stock, cost, price, fileName, supplierId, categoryId);

		if(masterUpdate == 0) {
			request.setAttribute("errMsg", "更新に失敗しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productmasterlistupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		response.sendRedirect("ProductMasterResult");
	}

}
