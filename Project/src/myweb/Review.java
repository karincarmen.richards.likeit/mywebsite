package myweb;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ReviewBeans;
import dao.ReviewDao;

/**
 * Servlet implementation class Review
 */
@WebServlet("/Review")
public class Review extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Review() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



		ReviewDao reviewDao = new ReviewDao();
		List<ReviewBeans> reviewList = reviewDao.findAll();

		System.out.println(reviewList);

		// リクエストスコープにマスタ一覧情報をセット
		HttpSession session = request.getSession();
		session.setAttribute("reviewList", reviewList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/review.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();


		String str = "";

		String title = request.getParameter("title");
		String detail = request.getParameter("reviewDetail");
		String fileName = request.getParameter("reviewPicture");
		String productId = request.getParameter("productId");

		if (title.equals(str) || detail.equals(str) || fileName.equals(str) ||productId.equals(str)) {
			request.setAttribute("errMsg", "空白を埋めてください");

			System.out.println("どこかが空白");

			List<ReviewBeans> reviewList = (List<ReviewBeans>) session.getAttribute("reviewList");
			session.setAttribute("reviewList", reviewList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/review.jsp");
			dispatcher.forward(request, response);
			return;

		}


		//新規登録
		ReviewDao reviewDao = new ReviewDao();
		int reviewRegist = reviewDao.registReview(title, detail, fileName, productId);
		//登録できてたら１、失敗だったら０
		System.out.println(reviewRegist);

		if(reviewRegist == 0) {
			request.setAttribute("errMsg", "登録に失敗しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/review.jsp");
			dispatcher.forward(request, response);
			return;
		}

		response.sendRedirect("ReviewResult");

	}

}
