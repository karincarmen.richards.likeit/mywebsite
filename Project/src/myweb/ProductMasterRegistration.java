package myweb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ProductDao;

/**
 * Servlet implementation class ProductMasterRegistration
 */
@WebServlet("/ProductMasterRegistration")
public class ProductMasterRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductMasterRegistration() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productmasterregistration.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String str = "";

		String name = request.getParameter("name");
		String detail = request.getParameter("detail");
		String stock = request.getParameter("stock");
		String cost = request.getParameter("cost");
		String price = request.getParameter("price");
		String fileName = request.getParameter("fileName");
		String supplierId = request.getParameter("supplierId");
		String categoryId = request.getParameter("categoryId");

		//空白確認
		if (name.equals(str) || detail.equals(str) || stock.equals(str) ||
				cost.equals(str)|| price.equals(str)|| fileName.equals(str)
				|| supplierId.equals(str)|| categoryId.equals(str)) {
			request.setAttribute("errMsg", "空白を埋めてください");

			System.out.println("どこかが空白");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productmasterregistration.jsp");
			dispatcher.forward(request, response);
			return;

		}

		//新規登録
		ProductDao productDao = new ProductDao();
		int product = productDao.registMaster(name, detail, stock, cost, price, fileName, supplierId, categoryId);


		if(product == 0) {
			request.setAttribute("errMsg", "登録に失敗しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productmasterregistration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		response.sendRedirect("ProductMasterResult");
	}

}
