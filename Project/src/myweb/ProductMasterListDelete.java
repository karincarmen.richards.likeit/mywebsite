package myweb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ProductBeans;
import dao.ProductDao;

/**
 * Servlet implementation class ProductMasterListDelete
 */
@WebServlet("/ProductMasterListDelete")
public class ProductMasterListDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductMasterListDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");

		ProductDao productDao = new ProductDao();
		ProductBeans produtmasterdetail = productDao.masterDetail(id);

		request.setAttribute("produtmasterdetail", produtmasterdetail);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productmasterlistdelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		ProductDao productDao = new ProductDao();
		int masterDelete = productDao.masterDelete(id);

		if(masterDelete == 0) {
			request.setAttribute("errMsg", "削除に失敗しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productmasterlistdelete.jsp");
			dispatcher.forward(request, response);
			return;
		}

		response.sendRedirect("ProductMasterResult");

	}

}
