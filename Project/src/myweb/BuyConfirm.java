package myweb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyBeans;
import beans.DeliveryMethodBeans;
import beans.ProductBeans;
import beans.UserBeans;
import dao.BuyDao;
import dao.DeliveryMethodDao;

/**
 * Servlet implementation class BuyConfirm
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		int selectDeliveryMethodId = Integer.parseInt(request.getParameter("deliveryMethodId"));
		if(selectDeliveryMethodId == 0) {
			System.out.println(selectDeliveryMethodId);
		}

		DeliveryMethodDao deliveryMethodDao = new DeliveryMethodDao();
		DeliveryMethodBeans userSelectDM = deliveryMethodDao.getDBMethodFromId(selectDeliveryMethodId);

		ArrayList<ProductBeans> cartList = (ArrayList<ProductBeans>) session.getAttribute("cart");

		int totalPrice = BuyDao.getTotalPrice(cartList) + userSelectDM.getPrice();

		BuyBeans buyData = new BuyBeans();
		buyData.setUserId(userInfo.getId());
		buyData.setTotalPrice(totalPrice);
		buyData.setDeliveryMethodId(userSelectDM.getId());
		buyData.setDeliveryMethodPrice(userSelectDM.getPrice());
		buyData.setDeliveryMethodName(userSelectDM.getName());


		session.setAttribute("buyData", buyData);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyconfirm.jsp");
		dispatcher.forward(request, response);

	}

}
