package myweb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class Registration
 */
@WebServlet("/Registration")
public class Registration extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registration() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registration.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String str = "";

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String address = request.getParameter("address");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");

		//空白確認
		if (loginId.equals(str) || name.equals(str) || birthDate.equals(str) ||address.equals(str)|| password.equals(str)) {
			request.setAttribute("errMsg", "空白を埋めてください");

			System.out.println("どこかが空白");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registration.jsp");
			dispatcher.forward(request, response);
			return;

		}

		//IDの重複確認
		UserDao checkIdDao = new UserDao();
		UserBeans user = checkIdDao.checkId(loginId);

		if(user!=null) {
			request.setAttribute("errMsg", "重複ID：入力されたIDは使用できません");

			System.out.println("どこかが空白");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(!password.equals(checkPassword)) {
			request.setAttribute("errMsg", "不一致：パスワードは同じものを入力してください");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registration.jsp");
			dispatcher.forward(request, response);
			return;

		}


		//新規登録
		UserDao userDao = new UserDao();
		int registUser = userDao.registUser(loginId, name, birthDate, address, password);
		//登録できてたら１、失敗だったら０
		System.out.println(registUser);

		if(registUser == 0) {
			request.setAttribute("errMsg", "登録に失敗しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		response.sendRedirect("RegistrationResult");
	}

}
