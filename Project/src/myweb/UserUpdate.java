package myweb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		UserBeans userDetail = userDao.userDetailInfo(id);


		session.setAttribute("userDetail", userDetail);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdateconfirm.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		String str = "";

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthdate = request.getParameter("birthdate");
		String address = request.getParameter("address");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");

		//空白確認
		if (name.equals(str) || birthdate.equals(str) ||address.equals(str)) {
			request.setAttribute("errMsg", "空白を埋めてください");

			System.out.println("どこかが空白");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdateconfirm.jsp");
			dispatcher.forward(request, response);
			return;

		}

		if(!password.equals(checkPassword)) {
			request.setAttribute("errMsg", "不一致：パスワードは同じものを入力してください");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdateconfirm.jsp");
			dispatcher.forward(request, response);
			return;

		}


		if(password.equals(str)) {
			UserDao userDao = new UserDao();
			int userUpdateWithoutPass = userDao.userUpdateWithoutPass(loginId, name, birthdate, address);

			if(userUpdateWithoutPass == 0) {
				request.setAttribute("errMsg", "更新に失敗しました");

				UserBeans userDetail = (UserBeans) session.getAttribute("userDetail");
				session.setAttribute("userDetail", userDetail);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdateconfirm.jsp");
				dispatcher.forward(request, response);
				return;

			}


		}else {
			UserDao userDao = new UserDao();
			int userUpdate = userDao.userUpdate(loginId, name, birthdate, address, password);

			System.out.println(userUpdate);

			if(userUpdate == 0) {
				request.setAttribute("errMsg", "更新に失敗しました");

				UserBeans userDetail = (UserBeans) session.getAttribute("userDetail");
				session.setAttribute("userDetail", userDetail);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdateconfirm.jsp");
				dispatcher.forward(request, response);
				return;

			}
		}

		response.sendRedirect("UserUpdateResult");
	}

}
