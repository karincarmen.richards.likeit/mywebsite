package myweb;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyBeans;
import beans.ProductBeans;
import beans.UserBeans;
import dao.BuyDao;
import dao.BuyDetailDao;

/**
 * Servlet implementation class UserBuyHistoryDetail
 */
@WebServlet("/UserBuyHistoryDetail")
public class UserBuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyHistoryDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");
		session.setAttribute("userInfo", userInfo);

		String buyId = request.getParameter("buy_id");

		BuyBeans buyInfo = BuyDao.getBuyInfoFromId(buyId);

		//商品詳細情報検索
		List<ProductBeans> buyDetail = BuyDetailDao.getBuyDataListFromBuyId(buyInfo.getId());


		request.setAttribute("buyInfo", buyInfo);
		request.setAttribute("buyDetail", buyDetail);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyhistory.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
