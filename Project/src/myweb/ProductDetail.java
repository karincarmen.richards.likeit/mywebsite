package myweb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ProductBeans;
import dao.ProductDao;

/**
 * Servlet implementation class ProductDetail
 */
@WebServlet("/ProductDetail")
public class ProductDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductDetail() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");

		ProductDao productDao = new ProductDao();
		ProductBeans productDetail = productDao.masterDetail(id);

		request.setAttribute("productDetail", productDetail);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productdetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		String id = request.getParameter("id");

		ProductDao productDao = new ProductDao();
		ProductBeans product = productDao.masterDetail(id);
		request.setAttribute("product", product);


		ArrayList<ProductBeans> cart = (ArrayList<ProductBeans>) session.getAttribute("cart");
		if(cart == null) {
			cart = new ArrayList<ProductBeans>();
		}
		cart.add(product);

		session.setAttribute("cart", cart);
		request.setAttribute("errMsg", "商品を追加しました");
		//RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productdetail.jsp");
		//dispatcher.forward(request, response);

		response.sendRedirect("Cart");
	}

}
