package myweb;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyBeans;
import beans.UserBeans;
import dao.BuyDao;
import dao.UserDao;

/**
 * Servlet implementation class User
 */
@WebServlet("/User")
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		session.setAttribute("userInfo", userInfo);

		int userId = userInfo.getId();
		System.out.println(userId);

		List<BuyBeans> buyHisotoryList = BuyDao.getbuyHistoryFromByuserId(userId);
		request.setAttribute("buyHisotoryList", buyHisotoryList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String userId = request.getParameter("id");

		UserDao userDao = new UserDao();
		UserBeans userDetail = userDao.userDetailInfo(userId);

		HttpSession session = request.getSession();
		session.setAttribute("userDetail", userDetail);


		response.sendRedirect("UserUpdate");

	}

}
