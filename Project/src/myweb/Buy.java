package myweb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DeliveryMethodBeans;
import beans.ProductBeans;
import beans.UserBeans;
import dao.DeliveryMethodDao;

/**
 * Servlet implementation class Buy
 */
@WebServlet("/Buy")
public class Buy extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Buy() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");
		ArrayList<ProductBeans> cart = (ArrayList<ProductBeans>) session.getAttribute("cart");

		if(userInfo == null) {
			response.sendRedirect("Login");
			return;

		}else if(cart.size() == 0) {
			request.setAttribute("errMsg", "購入する商品がありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);
			return;
		}else {
			DeliveryMethodDao deliveryMethodDao = new DeliveryMethodDao();
			List<DeliveryMethodBeans> deliveryMethodList = (ArrayList<DeliveryMethodBeans>) deliveryMethodDao.getDeliveryList();
			request.setAttribute("deliveryMethodList", deliveryMethodList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
