CREATE DATABASE new_myweb DEFAULT CHARACTER SET utf8;
 
USE new_myweb;

--ユーザ情報テーブル作成--
CREATE TABLE n_user(
id int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
login_id varchar(30) UNIQUE NOT NULL,
user_name varchar(30) NOT NULL,
birth_date date NOT NULL,
address varchar(256) NOT NULL,
password varchar(256) NOT NULL);
	
--情報入力--
INSERT INTO n_user VALUES(
	1,
	'admin',
	'管理者',
	'1994-11-01',
	'千葉県',
	'1101'
	);

--ブログテーブル作成--
CREATE TABLE blog(
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	user_id int NOT NULL,
	title varchar(256) NOT NULL,
	detail text,
	file_name varchar(256) DEFAULT NULL,
	create_date date NOT NULL);
	
--情報入力--
INSERT INTO blog VALUES(
	1,
	1,
	'試し',
	'あいうえおかきくけこさしすせそたちつてと',
	'',
	now()
	);
	
--購入テーブル作成--
CREATE TABLE buy(
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	total_price int NOT NULL,
	delivery_method_id int NOT NULL,
	user_id int NOT NULL,
	create_date date NOT NULL);
	
--情報入力--
INSERT INTO buy(id, total_price, delivery_method_id, user_id, create_date)VALUE
	(1, 1234, 1, 1, now());
	
--配送テーブル作成--
CREATE TABLE delivery_method(
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	name varchar(30) UNIQUE NOT NULL,
	price int(11) NOT NULL
	);
	
--情報入力--
INSERT INTO delivery_method(id, name, price)VALUE
	(1, '翌日配送', 1000),
	(2, '通常配送', 0),
	(3, '日時指定配送', 500);

--購入詳細テーブル作成--
CREATE TABLE buy_detail(
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	buy_id int NOT NULL,
	product_id int NOT NULL,
	create_date date NOT NULL
	);
	
--情報入力--
INSERT INTO buy_detail(id, buy_id, product_id, create_date)VALUE
	(1, 2, 3, now());

--商品テーブル作成--
CREATE TABLE product(
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	name varchar(50) NOT NULL,
	detail text,
	stock int NOT NULL,
	cost int NOT NULL,
	price int NOT NULL,
	file_name varchar(50) DEFAULT NULL,
	supplier_id int NOT NULL,
	category_id int NOT NULL,
	product_id int NOT NULL);
	
	
--情報入力--
INSERT INTO product(id, name, detail, stock, cost, price, file_name, supplier_id, category_id) VALUES 
	(1, '試供品', 'これは試供品です。', 1, 100, 0, 'cookie1.jpeg', 1, 5),
	(2, 'クッキー', 'カラダに無理のない素材。 「厳選素材」 マクロビ仕様のこだわりヘルシークッキー。 ', 1, 1980, 2420, 'cookie1.jpeg', 2, 4),
	(3, 'クッキー', 'カラダに無理のない素材。 「厳選素材」 マクロビ仕様のこだわりヘルシークッキー。 ', 1, 1980, 2420, 'cookie1.jpeg', 2, 4),
	(4, 'クッキー', 'カラダに無理のない素材。 「厳選素材」 マクロビ仕様のこだわりヘルシークッキー。 ', 1, 1980, 2420, 'cookie1.jpeg', 2, 4),
	(5, 'ヴィーガンヌードル 担担麺(3個セット)', 'ビーガンでも食べれるタンタンメン。植物性由来の原料のみ使用しています。', 1, 900, 1430, 'noodles1.jpeg', 3, 2),
	(6, 'オーガニックプロテインパウダー', '西日本産、北海道産、または神奈川県産の小松菜です。自然農法、または有機農法。', 1, 300, 400, 'fresh1.jpeg', 1, 1),
	(7, '国産雑穀と玄米のごはん【レトルト】', '厳選した5種類の雑穀をバランスよくミックスして炊き上げた、レトルトタイプの玄米ごはんです。', 1, 400, 520, 'rice1.jpeg', 3, 2),
	(8, 'おいものかりんとう（さつまいも）', '徹底的に素材を厳選し、余計なものを使わず、素材のおいしさをそのまま生かしています。', 1, 400, 550, 'protein.png', 2, 4);

	
--レビューテーブル作成--
CREATE TABLE review(
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	title varchar(256) NOT NULL,
	detail text,
	file_name varchar(30) DEFAULT NULL,
	product_id int NOT NULL,
	create_date date NOT NULL);
	
--情報入力--
INSERT INTO review VALUES(
	1,
	'レビュー',
	'買ってよかったです。',
	'',
	1,
	now()
	);


--仕入れ先テーブル作成--
CREATE TABLE supplier(
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	supplier_name varchar(30) UNIQUE NOT NULL
	);
	
--情報入力--
INSERT INTO supplier(id, supplier_name)VALUE
	(1, 'くだもの農園'),
	(2, '株式会社だいち'),
	(3, 'シリアル有限会社'),
	(4, 'オーガニック協会');

--商品区分テーブル作成--
CREATE TABLE category(
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	category_name varchar(30) UNIQUE NOT NULL
	);
	
--情報入力--
INSERT INTO category(id, category_name)VALUE
	(1, '新鮮食品'),
	(2, '米・穀類'),
	(3, '調味料'),
	(4, 'お菓子'),
	(5, 'その他');
	

--お問い合わせテーブル作成--
CREATE TABLE contact(
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	title varchar(30) NOT NULL,
	detail text,
	create_date date NOT NULL);
	
--情報入力--
INSERT INTO contact VALUES(
	1,
	'質問',
	'あああああああああああああああああああああああ',
	now()
	);
